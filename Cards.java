import java.util.Random;
public class Cards {
		public String[] SUITS = { "Clubs", "Diamonds", "Hearts", "Spades"};
		public String[] RANKS = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"};
		
		public static void main(String[] args)
		{
			Cards c = new Cards();
			c.allCards();
		}
		public void allCards() {
			String [] deck = new String[52];
			
			int i,j,k=0;
			for(i=0;i<SUITS.length;i++)
			{
				for(j=0;j<RANKS.length;j++)
				{
					deck[k]=(SUITS[i] + " of " + RANKS[j]);
					k++;
				}
			}
			printCards(deck);
			shuffleCards(deck);
		}
		public void printCards(String deck[]) {
			int i;
			for(i=0;i<deck.length;i++)
			{
				System.out.println(deck[i]);
			}
		}
		public void shuffleCards(String deck[])
		{
			Random r = new Random();
			int randomIndexToSwap;
			String temp;
			for (int i = 0; i < deck.length; i++) 
			{
				randomIndexToSwap = r.nextInt(deck.length);
				temp = deck[randomIndexToSwap];
				deck[randomIndexToSwap] = deck[i];
				deck[i] = temp;
			}
			printShuffledCards(deck);
		}
		public void printShuffledCards(String deck[])
		{
			int i;
			System.out.println("\n\nSHUFFLED CARDS ARE\n");
			for(i=0;i<deck.length;i++)
			{
				System.out.println(deck[i]);
			}
		}
}
		
